#!/usr/bin/env bash

NAME="rockbugs"
VERSION="0.0.1"
EMAIL="serg.soloviev@gmail.com"
DESCRIPTION="tektorg test"
#-------------------------------------------------------------------------------
echo "building for x64:"
go get ./...
GOOS=linux GOARCH=amd64 go build -v -o "$NAME"_linux main.go
GOOS=windows GOARCH=amd64 go build -v -o "$NAME"_windows.exe main.go
GOOS=darwin GOARCH=amd64 go build -v -o "$NAME"_mac main.go
#-------------------------------------------------------------------------------
