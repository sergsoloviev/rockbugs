package main

import (
	"fmt"
	"os"
)

var (
	intervals  [][]int
	leftCount  int
	rightCount int
	errorText  = "ROCKS and BUGS must be unsigned integer numbers, " +
		"number of BUGS must be lower or equal to ROCKS"
)

func answer() {
	fmt.Printf("left: %d, right: %d\n", leftCount, rightCount)
}

func is_ok(rocks int, bugs int) bool {
	if rocks <= 0 || bugs <= 0 {
		fmt.Print(errorText)
		return false
	}
	if bugs > rocks {
		fmt.Println(errorText)
		return false
	}
	return true
}

func is_zero_interval(interval []int) bool {
	for _, v := range interval {
		if v != 0 {
			return false

		}
	}
	return true
}

func get_max_len_interval() (int, int, []int) {
	max := 0
	key := 0
	for k, v := range intervals {
		if !is_zero_interval(v) {
			continue
		}
		length := len(v)
		if length > max {
			max = length
			key = k
		}
	}
	return key, max, intervals[key]
}

func main() {
	var ROCKS, BUGS int
	fmt.Printf("ROCKS: ")
	fmt.Fscan(os.Stdin, &ROCKS)
	fmt.Printf("BUGS: ")
	fmt.Fscan(os.Stdin, &BUGS)
	if !is_ok(ROCKS, BUGS) {
		return
	}

	rocksList := make([]int, ROCKS)
	intervals = append(intervals, rocksList)
	useless := (ROCKS / 2) + 1
	if BUGS > useless {
		answer()
		return
	}
	for b := 1; b <= BUGS; b++ {
		k, l, interval := get_max_len_interval()
		cell := l / 2
		if l%2 == 0 {
			cell--
		}
		interval[cell] = b
		leftInterval := interval[0:cell]
		rightInterval := interval[cell+1 : l]
		leftCount = len(leftInterval)
		rightCount = len(rightInterval)
		intervals = append(intervals[:k], intervals[k+1:]...)
		intervals = append(intervals, leftInterval, rightInterval)
		fmt.Printf("%d %v\n", b, rocksList)
	}
	answer()
	return
}
